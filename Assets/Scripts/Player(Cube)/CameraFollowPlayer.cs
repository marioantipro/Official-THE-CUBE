﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{

    public GameObject giocatore;

    private Vector3 offset = new Vector3(0, 5, -9);
    // I 3 numeri che si trovano dentro la parentesi sono la posizione della camera quando segue il giocatore

    void Start()
    {
        
    }

    void Update()
    {
        transform.position = giocatore.transform.position + offset;
    }
}
