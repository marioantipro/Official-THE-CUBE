﻿using UnityEngine;

public class CarAnimation : MonoBehaviour
{
    [SerializeField] private Vector3 finalPosition;
    private Vector3 initialPosition;
    private void Awake()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, finalPosition, 0.1f);
        // Il numero alla fine della parentesi che si trova qui sopra indica la velocità in cui il cubo fa l'animazione di apparire
    }

    private void OnDisable()
    {
        transform.position = initialPosition;
    }
}
