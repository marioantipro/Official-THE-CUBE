﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayGameInSelectionCube : MonoBehaviour
{
    public void LoadLevelNumber(int _index)
    {
        SceneManager.LoadScene(_index);
    }
}
